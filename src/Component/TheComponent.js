import React from 'react'
import './theComponent.css'

const poste = ['Account Executive', 'Account Manager', 'Administrateur système', 'Développeur android', 'Développeur backend', 'Business developer', 'CEO', 'Chef de projet', 'Marketing (CMO)', 'Chef opérateur (COO)', 'Chef technique (CTO)', 'Custom succes manager', 'Data engineer', 'Data scientist', 'Dev ops', 'Développeur fullstack', 'Développeur front', 'Growth', 'Lead dev', 'Développeur mobile','Développeur ios', 'Product manager', 'Software engineer', 'Talent manager', 'Designer'];

const searchWithKey = [{
    titre: 'Account Executive',
    motClef: /account executive/i
}, {
    titre: 'Account Manager',
    motClef: /account manager/i
},{
    titre: 'Administrateur système',
    motClef: /administrateur système|admin système|admin systeme|admin system/i
},{
    titre: 'Développeur android',
    motClef: /android/i
},{
    titre: 'Développeur backend',
    motClef: /back/i
},{
    titre: 'Business developer',
    motClef: /business developer|business développeur|business developpeur/i
},{
    titre: 'CEO',
    motClef: /ceo|chief executive|chef d'éxecution|chef d'execution/i
},{
    titre: 'Chef de projet',
    motClef: /chef de projet|project chief|cdp/i
},{
    titre: 'Marketing (CMO)',
    motClef: /marketing/i
},{
    titre: 'Chef opérateur (COO)',
    motClef: /opérateur|operateur|opération|operation|coo/i
},{
    titre: 'Chef technique (CTO)',
    motClef: /CTO/
},{
    titre: 'Custom succes manager',
    motClef: /custom/i
},{
    titre: 'Data engineer',
    motClef: /Data engineer/i
},{
    titre: 'Data scientist',
    motClef: /scientist/i
},{
    titre: 'Dev ops',
    motClef: /dev ops|devops/i
},{
    titre: 'Développeur fullstack',
    motClef: /full/i
},{
    titre: 'Développeur front',
    motClef: /front/i
},{
    titre: 'Growth',
    motClef: /Growth/i
},{
    titre: 'Lead dev',
    motClef: /lead/i
},{
    titre: 'Développeur mobile',
    motClef: /mobile/i
},{
    titre: 'Développeur ios',
    motClef: /ios/i
},{
    titre: 'Product manager',
    motClef: /product manager|product manageur/i
},{
    titre: 'Software engineer',
    motClef: /software en/i
},{
    titre: 'Talent manager',
    motClef: /talent/i
},{
    titre: 'Designer',
    motClef: /design/i
}]

const link = 'https://pure-brushlands-28319.herokuapp.com';
// const link = 'http://localhost:5000';

export default class TheComponent extends React.Component{

    constructor(props){
        super(props)
        this.workText = '';
        this.xpText = '';
        this.salaryText = '';
        this.addWorkText = '';
        this.state = {
            data:[],
            salaryJunior:[],
            salaryIntermediaire:[],
            salarySenior:[],
            averageJunior:[0],
            averageIntermediaire:[0],
            averageSenior:[0],
            errorSalary : '',
            formDone: false,
            formAddWork:false,
            formAddWorkDone:false
        }
    }

    getData=()=>{
        fetch(`${link}/result-salary`)
        .then(res=>res.json())
        .then(data=>this.setState({data:data['Salaires Français']},()=>this.initializeArray()))
    }

    initializeArray=()=>{
        const data = this.state.data;
        //on parcourt le tableau
        for(var i=2;i<data.length;i++){
            //on parcourt le searchWithKey pour trouver les mot clefs et changer les intitulé de poste
            for(var j=0;j<searchWithKey.length;j++){
                if(data[i].B.match(searchWithKey[j].motClef)){
                    //Si un mot clef est trouvé, l'intitulé est remplacé
                    data[i].B = searchWithKey[j].titre;
                }
            }
        }
    }

    posteWasChoose=(p1)=>{
        const data = this.state.data;
        //Le poste a été choisi, maintenant on parcourt le data pour récupérer les salaire et les mettre dans le state salary
        for(var i=2;i<data.length;i++){
            if(data[i].B === p1 && typeof data[i].E === 'number'){
                if(data[i].A === 'Junior'){
                    this.state.salaryJunior.push(data[i].E);
                }
                if(data[i].A === 'Intermédiaire'){
                    this.state.salaryIntermediaire.push(data[i].E);
                }
                if(data[i].A === 'Avancé'){
                    this.state.salaryIntermediaire.push(data[i].E);
                }
                if(data[i].A === 'Experimenté'){
                    this.state.salarySenior.push(data[i].E);
                }
                if(data[i].A === 'Master'){
                    this.state.salarySenior.push(data[i].E);
                }
            }
        }
            this.showTheResult();
    }

    showTheResult = () => {
        // console.log(this.state)
        //ici on fait la moyenne de state salary et on l'affiche. Si c'est vide, on affiche une phrase
        const noData = 'Aucune données';

        let averageJunior=()=>{
            if(this.state.salaryJunior.length > 0){
                let ave = this.state.salaryJunior.reduce((a, b) => a + b, 0) / this.state.salaryJunior.length;
                return ave.toFixed() + '€'
            } else {
                return noData
            }
        }

        let averageIntermediaire=()=>{
            if(this.state.salaryIntermediaire.length > 0){
                let ave = this.state.salaryIntermediaire.reduce((a, b) => a + b, 0) / this.state.salaryIntermediaire.length;
                return ave.toFixed() + '€'
            } else {
                return noData
            }
        }

        let averageSenior=()=>{
            if(this.state.salarySenior.length > 0){
                let ave = this.state.salarySenior.reduce((a, b) => a + b, 0) / this.state.salarySenior.length;
                return ave.toFixed() + '€'
            } else {
                return noData
            }
        }

        this.setState({
            averageJunior:          averageJunior(),
            salaryJunior:           [],
            averageIntermediaire:   averageIntermediaire(),
            salaryIntermediaire:    [],
            averageSenior:          averageSenior(),
            salarySenior:           []
        })
    }

    handleWork = e => {
        this.workText = e.target.value;
    }

    handleXp = e => {
        this.xpText = e.target.value;
    }

    handleSalary = e => {
        this.salaryText = e.target.value
    }

    checkData=(event)=>{
        event.preventDefault()
        this.salaryText.length < 5 || this.salaryText.length > 6 ? 
            this.setState({errorSalary:'Pas le bon format'})
            :
            this.sendData();
    }

    sendData = async e => {
        const response = await fetch(`${link}/sendData`, {
            method: 'POST',
            headers: {
                'content-Type': 'application/json'
            },
            body: JSON.stringify({
                data: {
                    work: this.workText,
                    xpText:this.xpText,
                    salaryText:parseFloat(this.salaryText)
                }
            }),
        });
        const body = await response.json();
        console.log(body)
        if (body.message === true) {
            this.setState({formDone:true})
        } else {
            console.log('no')
        }
    }

    handleAddWork = e => {
        this.addWorkText = e.target.value
    }

    addWorkFunction = (e) => {
        e.preventDefault();
        this.addWorkText.length < 3 || this.addWorkText.length > 40 ?
            console.log('noooo')
            :
            this.sendDataAddWork()
    }

    sendDataAddWork = async e => {
        const response = await fetch(`${link}/sendaddwork`, {
            method: 'POST',
            headers: {
                'content-Type': 'application/json'
            },
            body: JSON.stringify({
                data: {
                    addWork: this.addWorkText,
                }
            }),
        });
        const body = await response.json();
        console.log(body)
        if (body.message === true) {
             this.setState({formAddWorkDone:true})
        } else {
            console.log('no')
        }
        
    }

    componentWillMount(){
        this.getData();
    }

    componentDidUpdate(){
        // console.log(this.state)
    }
    

    render(){

        return(
            <div className='compo'>
                <div className={this.state.formAddWork ? 'compoDivAddWork': 'compoDiv'}>
                    <h1>Connaitre la moyenne des salaires IT en France <span style={{fontSize:'12px'}}>BETA</span> </h1>
                    <h4>Basé sur des centaines de témoignagnes anonymes</h4>
                    <div className='listSalaryDiv'>
                        <div className='salary'>Junior <p>{this.state.averageJunior}</p></div>
                        <div className='salary'>Intermédiaire <p>{this.state.averageIntermediaire}</p></div>
                        <div className='salary'>Senior <p> {this.state.averageSenior}</p></div>
                        {/* <div className='salary'>Expert <p>{this.state.averageExpert}</p> </div> */}
                        {/* <div className='salary'>Master <p> {this.state.averageMaster}</p></div> */}
                    </div>
                    <div>
                        <h5>Salaire en brut par an</h5>
                    </div>
                    <form>
                        {poste.map((item,index)=>
                        <div key={index} className='input-radio-question'>
                            <div className='container'>
                                <div className='radio-tile-group'>
                                    <div className='input-container'>
                                        <input key={index} type="radio" name='poste' id={item}
                                            onClick={e=>this.posteWasChoose(item)} className='radio-button' value={item}
                                        />
                                        <div className='radio-tile'>
                                            <label className='radio-tile-label'> {item} </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        )}
                    </form>
                    <div>
                        <p className='infoBase'>Infos basés sur le fichier excel de Rodolphe@remotive.io et sur la base de donnée de salaire-tech.netlify.com</p>
                    </div>
                    <br/>
                    <div>
                        <div>
                            <p className='hesitezPas'>N'hésitez pas à alimenter la base de donnée en donnant anonymement votre salaire</p>
                        </div>
                        <div>
                            {!this.state.formAddWork && 
                            <form className='form' onSubmit={this.checkData}>
                                <p>Votre emploi</p>
                                {searchWithKey.map((item,index)=>
                                <div key={index}>
                                    <input type="radio" name="work" id="" value={item.titre} onChange={this.handleWork} required/>
                                    <label>{item.titre}</label>
                                    <br/>
                                    </div>
                                )}
                                <br/>
                                <div style={{fontSize:'12px'}}>Votre emploi ne figure pas dans la liste ? <span onClick={e=>this.setState({formAddWork:true})} style={{cursor:'pointer'}}>Cliquez ici</span> </div>

                                <p>Votre niveau d'experience</p>
                                <select onChange={this.handleXp} required>
                                    <option value="" selected disabled hidden>Faites votre choix</option>
                                    <option value="junior">Junior (1 à 2 ans)</option>
                                    <option value="intermediaire">Intermédiaire (2 à 5 ans)</option>
                                    <option value="senior">Senior (5 ans et +)</option>
                                </select>

                                <p>Votre salaire (brut par an)</p>
                                <input type="number" onChange={this.handleSalary} required/>
                                {this.state.errorSalary}
                                <br/>
                                <br/>
                                <input type="submit" value="Envoyer" disabled={this.state.formDone}/>
                                <br/>
                                {this.state.formDone && <p style={{textAlign:'center',color:'red'}}>Merci d'avoir rempli le formulaire et aider la communauté</p>}
                            </form>
                            }
                            {this.state.formAddWork && 
                            <div>
                                <form className='form' onSubmit={this.addWorkFunction}>
                                    <label>Quel est l'emploi manquant ?   </label>
                                    <input type="text" name="" id="" onChange={this.handleAddWork} required/>
                                    <br/>
                                    <br/>
                                    <input type="submit" value='Envoyer' disabled={this.state.formAddWorkDone}/>
                                    <br/>
                                    {this.state.formAddWorkDone && <p style={{textAlign:'center',color:'red'}}>Merci pour la proposition, nous allons certainement ajouter cet emploi dans la liste</p>}
                                </form>
                            </div>
                            }
                            <br/>
                            <br/>
                            <div>
                                <p style={{textAlign:'center'}}>Site en BETA, crée par Djoher Khaled, développeur Fullstack <a href='https://www.linkedin.com/in/djoher-khaled-b51388b0'>(voir le linkedin)</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}